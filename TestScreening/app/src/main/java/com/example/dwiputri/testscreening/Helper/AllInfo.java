package com.example.dwiputri.testscreening.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Dwi Putri on 4/18/2016.
 */
public class AllInfo {

    static final String USER_NAME = "user_name";
    static final String EVENT_NAME = "event_name";
    static final String GUEST_NAME = "guest_name";

    static SharedPreferences getSharedPreferences(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    /* use after log in*/
    public static void setUserName(Context context, String user_name){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_NAME, user_name);
        editor.commit();
    }

    public static String getUserName(Context context){
        return getSharedPreferences(context).getString(USER_NAME, "");
    }

    /* use after choose event*/
    public static void setEventName(Context context, String event_name){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(EVENT_NAME, event_name);
        editor.commit();
    }

    public static String getEventName(Context context){
        return getSharedPreferences(context).getString(EVENT_NAME, "");
    }

    /* use after choose guest*/
    public static void setGuestName(Context context, String guest_name){
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(GUEST_NAME, guest_name);
        editor.commit();
    }

    public static String getGuestName(Context context){
        return getSharedPreferences(context).getString(GUEST_NAME, "");
    }
}
