package com.example.dwiputri.testscreening.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.dwiputri.testscreening.Adapter.CustomEventAdapter;
import com.example.dwiputri.testscreening.Helper.AllInfo;
import com.example.dwiputri.testscreening.Model.ModelEvent;
import com.example.dwiputri.testscreening.R;

import java.util.ArrayList;

public class EventActivity extends AppCompatActivity {
    ListView list;
    CustomEventAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        adapter = new CustomEventAdapter(this, generateData());
        list = (ListView) findViewById(R.id.list_item);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                ModelEvent selectedItem = (ModelEvent) adapter.getItem(position);
                Toast.makeText(getApplicationContext(), selectedItem.getEventName(), Toast.LENGTH_SHORT).show();
                AllInfo.setEventName(getApplicationContext(), selectedItem.getEventName());
                AllInfo.setGuestName(getApplicationContext(), "NOT SET");
                Intent nextScreen = new Intent(getApplicationContext(), EventGuestActivity.class);
                Log.e("name", selectedItem.getEventName());
                startActivity(nextScreen);
            }
        });
    }

    private ArrayList<ModelEvent> generateData(){
        ArrayList<ModelEvent> modelEvents = new ArrayList<ModelEvent>();
        modelEvents.add(new ModelEvent(R.drawable.rounded, "Workshop A", "10 Maret 2016"));
        modelEvents.add(new ModelEvent(R.drawable.rounded, "General Lecture  B", "20 Maret 2016"));
        modelEvents.add(new ModelEvent(R.drawable.rounded, "Workshop C", "30 Maret 2016"));
        modelEvents.add(new ModelEvent(R.drawable.rounded, "General Lecture  D", "4 April 2016"));
        modelEvents.add(new ModelEvent(R.drawable.rounded, "Code Warrior Competition E", "10 April 2016"));
        modelEvents.add(new ModelEvent(R.drawable.rounded, "Android Submission F", "18 April 2016"));

        return modelEvents;
    }
}
