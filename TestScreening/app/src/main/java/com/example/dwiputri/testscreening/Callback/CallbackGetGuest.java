package com.example.dwiputri.testscreening.Callback;

import com.example.dwiputri.testscreening.Model.ModelGuest;

import java.util.List;

/**
 * Created by Dwi Putri on 4/18/2016.
 */
public class CallbackGetGuest {
    List<ModelGuest> items;

    public List<ModelGuest> getItems() {
        return items;
    }

    public void setItems(List<ModelGuest> items) {
        this.items = items;
    }
}
