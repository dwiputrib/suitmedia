package com.example.dwiputri.testscreening.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.dwiputri.testscreening.Model.ModelGuest;
import com.example.dwiputri.testscreening.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dwi Putri on 4/18/2016.
 */
public class CustomGuestAdapter extends BaseAdapter {
    List<ModelGuest> m_listGuest = new ArrayList<>();
    Context m_context;
    LayoutInflater m_layoutInflater;

    public CustomGuestAdapter(Context context, List<ModelGuest> list){
        this.m_listGuest = list;
        this.m_context = context;
        this.m_layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return m_listGuest.size();
    }

    @Override
    public Object getItem(int position) {
        return m_listGuest.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = m_layoutInflater.inflate(R.layout.custom_guest, null);
            holder = new ViewHolder();

            holder.name = (TextView) convertView.findViewById(R.id.tvName);
            holder.birthdate = (TextView) convertView.findViewById(R.id.tvBirthdate);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.name.setText(m_listGuest.get(position).getName());
        holder.birthdate.setText(m_listGuest.get(position).getBirthdate());

        return convertView;
    }

    static class ViewHolder {
        TextView name;
        TextView birthdate;
    }
}
