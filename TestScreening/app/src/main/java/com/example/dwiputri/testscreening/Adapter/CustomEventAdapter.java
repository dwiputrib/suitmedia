package com.example.dwiputri.testscreening.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dwiputri.testscreening.Model.ModelEvent;
import com.example.dwiputri.testscreening.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dwi Putri on 4/18/2016.
 */
public class CustomEventAdapter extends BaseAdapter {
    List<ModelEvent> m_listEvent = new ArrayList<>();
    Context m_context;
    LayoutInflater m_layoutInflater;

    // TODO Auto-generated constructor stub
    public CustomEventAdapter(Context context, List<ModelEvent> list){
        this.m_listEvent = list;
        this.m_context = context;
        this.m_layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return m_listEvent.size();
    }

    @Override
    public Object getItem(int position) {
        return m_listEvent.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position,View rowView,ViewGroup parent) {
        final ViewHolder holder;

        if (rowView == null) {
            rowView = m_layoutInflater.inflate(R.layout.custom_event, null);
            holder = new ViewHolder();

            holder.img = (ImageView) rowView.findViewById(R.id.icon);
            holder.eventname = (TextView) rowView.findViewById(R.id.tvEvent);
            holder.eventdate = (TextView) rowView.findViewById(R.id.tvDate);

            rowView.setTag(holder);

        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        holder.img.setImageResource(m_listEvent.get(position).getImg());
        holder.eventname.setText(m_listEvent.get(position).getEventName());
        holder.eventdate.setText(m_listEvent.get(position).getEventDate());

        return rowView;
    }

    static class ViewHolder {
        ImageView img;
        TextView eventname;
        TextView eventdate;
    }
}
