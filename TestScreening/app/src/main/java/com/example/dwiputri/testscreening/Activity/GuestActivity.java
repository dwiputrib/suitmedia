package com.example.dwiputri.testscreening.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.example.dwiputri.testscreening.Adapter.CustomGuestAdapter;
import com.example.dwiputri.testscreening.Helper.AllInfo;
import com.example.dwiputri.testscreening.Helper.RESTClient;
import com.example.dwiputri.testscreening.Model.ModelGuest;
import com.example.dwiputri.testscreening.R;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;

public class GuestActivity extends AppCompatActivity {
    ProgressDialog progressDialog;

    GridView gridView;
    CustomGuestAdapter m_adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest);

        progressDialog = new ProgressDialog(GuestActivity.this);
        progressDialog.setMessage("Fetching data...");
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        gridView = (GridView) findViewById(R.id.gridView);


        RESTClient.getRestClient(getApplicationContext()).getGuest(new Callback<List<ModelGuest>>() {
            @Override
            public void success(final List<ModelGuest> modelGuests, retrofit.client.Response response) {
                Log.i("size", String.valueOf(modelGuests.size()) + modelGuests.get(1).getBirthdate());

                m_adapter = new CustomGuestAdapter(getApplicationContext(), modelGuests);
                gridView.setAdapter(m_adapter);

                progressDialog.dismiss();

                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {


                        String birthdate = modelGuests.get(position).getBirthdate();
                        String[] parts = birthdate.split("\\-"); // escape -

                        String date = parts[2];

                        int _date = Integer.parseInt(date);
                        //Log.i("MYINT", "Value:  " +  _date);
                        if (_date % 2 == 0 && _date % 3 == 0) {
                            Toast.makeText(getApplicationContext(), "iOS", Toast.LENGTH_SHORT).show();
                            AllInfo.setGuestName(getApplicationContext(), modelGuests.get(position).getName());
                            AllInfo.setEventName(getApplicationContext(), "NOT SET");
                            Intent nextScreen = new Intent(getApplicationContext(), EventGuestActivity.class);
                            //nextScreen.putExtra("button_name", modelGuests.get(position).getName());
                            Log.e("name", modelGuests.get(position).getName());
                            startActivity(nextScreen);
                        } else if (_date % 2 == 0) {
                            Toast.makeText(getApplicationContext(), "Blackberry", Toast.LENGTH_SHORT).show();
                            AllInfo.setGuestName(getApplicationContext(), modelGuests.get(position).getName());
                            AllInfo.setEventName(getApplicationContext(), "NOT SET");
                            Intent nextScreen = new Intent(getApplicationContext(), EventGuestActivity.class);
                            //nextScreen.putExtra("button_name", modelGuests.get(position).getName());
                            Log.e("name", modelGuests.get(position).getName());
                            startActivity(nextScreen);
                        } else if (_date % 3 == 0) {
                            Toast.makeText(getApplicationContext(), "Android", Toast.LENGTH_SHORT).show();
                            AllInfo.setGuestName(getApplicationContext(), modelGuests.get(position).getName());
                            AllInfo.setEventName(getApplicationContext(), "NOT SET");
                            Intent nextScreen = new Intent(getApplicationContext(), EventGuestActivity.class);
                            //nextScreen.putExtra("button_name", modelGuests.get(position).getName());
                            Log.e("name", modelGuests.get(position).getName());
                            startActivity(nextScreen);
                        } else {
                            Toast.makeText(getApplicationContext(), "Phone", Toast.LENGTH_SHORT).show();
                            AllInfo.setGuestName(getApplicationContext(), modelGuests.get(position).getName());
                            AllInfo.setEventName(getApplicationContext(), "NOT SET");
                            Intent nextScreen = new Intent(getApplicationContext(), EventGuestActivity.class);
                            //nextScreen.putExtra("button_name", modelGuests.get(position).getName());
                            Log.e("name", modelGuests.get(position).getName());
                            startActivity(nextScreen);
                        }
                    }
                });

            }

            @Override
            public void failure(RetrofitError error) {
                Log.i("error", error.getMessage());
            }
        });
    }
}
