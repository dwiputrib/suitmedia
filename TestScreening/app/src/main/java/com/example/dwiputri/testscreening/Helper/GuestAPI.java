package com.example.dwiputri.testscreening.Helper;

import com.example.dwiputri.testscreening.Model.ModelGuest;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by Dwi Putri on 4/18/2016.
 */
public interface GuestAPI {
    @GET("/people")
    void getGuest(Callback<List<ModelGuest>> callback);
}
