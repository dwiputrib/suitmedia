package com.example.dwiputri.testscreening.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.dwiputri.testscreening.Helper.AllInfo;
import com.example.dwiputri.testscreening.R;

public class EventGuestActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnEventScreen;
    private Button btnGuestScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_guest);

        btnEventScreen = (Button) findViewById(R.id.btn_event);
        btnEventScreen.setOnClickListener(this);
        btnGuestScreen = (Button) findViewById(R.id.btn_guest);
        btnGuestScreen.setOnClickListener(this);
        Intent intentName = getIntent();

        if(AllInfo.getEventName(getApplicationContext()).equals("NOT SET")){
            btnEventScreen.setText("CHOOSE EVENT");
        }
        else{
            btnEventScreen.setText(AllInfo.getEventName(getApplicationContext()) + " EVENT");
        }

        if(AllInfo.getGuestName(getApplicationContext()).equals("NOT SET")){
            btnGuestScreen.setText("CHOOSE GUEST");
        }
        else{
            btnGuestScreen.setText(AllInfo.getGuestName(getApplicationContext()));
        }

        TextView txtName = (TextView) findViewById(R.id.tv_name2);
        txtName.setText(AllInfo.getUserName(getApplicationContext()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_event:
                Intent event = new Intent(getApplicationContext(),
                        EventActivity.class);
                startActivity(event);
                break;
            case R.id.btn_guest:
                Intent guest = new Intent(getApplicationContext(),
                        GuestActivity.class);
                startActivity(guest);
                break;
            default:
                break;
        }
    }
}
