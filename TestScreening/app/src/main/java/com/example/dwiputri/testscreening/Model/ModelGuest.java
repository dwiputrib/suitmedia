package com.example.dwiputri.testscreening.Model;

/**
 * Created by Dwi Putri on 4/18/2016.
 */
public class ModelGuest {
    int id;
    String name;
    String birthdate;

    public void setId(int id){
        this.id = id;
    }

    public int getId(){
        return this.id;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setBirthdate(String birthdate){
        this.birthdate = birthdate;
    }

    public String getBirthdate(){
        return this.birthdate;
    }
}
