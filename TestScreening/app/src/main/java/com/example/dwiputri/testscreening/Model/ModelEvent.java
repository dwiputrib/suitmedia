package com.example.dwiputri.testscreening.Model;


/**
 * Created by Dwi Putri on 4/18/2016.
 */
public class ModelEvent {
    int img;
    String eventname;
    String eventdate;

    public ModelEvent(int img, String eventname, String eventdate){
        this.img = img;
        this.eventname = eventname;
        this.eventdate = eventdate;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public void setEventName(String eventname){
        this.eventname = eventname;
    }

    public String getEventName(){
        return this.eventname;
    }

    public void setEventDate(String eventdate){
        this.eventdate = eventdate;
    }

    public String getEventDate(){
        return this.eventdate;
    }
}
