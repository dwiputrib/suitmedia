package com.example.dwiputri.testscreening.Callback;

import com.example.dwiputri.testscreening.Model.ModelEvent;

import java.util.List;

/**
 * Created by Dwi Putri on 4/18/2016.
 */
public class CallbackGetEvent {
    List<ModelEvent> items;

    public List<ModelEvent> getItems() {
        return items;
    }

    public void setItems(List<ModelEvent> items) {
        this.items = items;
    }
}
