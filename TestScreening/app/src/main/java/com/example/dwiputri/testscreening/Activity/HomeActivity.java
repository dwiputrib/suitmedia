package com.example.dwiputri.testscreening.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.dwiputri.testscreening.Helper.AllInfo;
import com.example.dwiputri.testscreening.R;

public class HomeActivity extends AppCompatActivity {
    private EditText inputName = null;
    private Button btnNextScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        inputName = (EditText) findViewById(R.id.et_name);
        btnNextScreen = (Button) findViewById(R.id.btn_next);

        btnNextScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nextScreen = new Intent(getApplicationContext(), EventGuestActivity.class);
                AllInfo.setUserName(getApplicationContext(), inputName.getText().toString());
                AllInfo.setGuestName(getApplicationContext(), "NOT SET");
                AllInfo.setEventName(getApplicationContext(), "NOT SET");
                nextScreen.putExtra("name", inputName.getText().toString());
                Log.e("n", inputName.getText().toString());
                startActivity(nextScreen);
            }
        });
    }
}
